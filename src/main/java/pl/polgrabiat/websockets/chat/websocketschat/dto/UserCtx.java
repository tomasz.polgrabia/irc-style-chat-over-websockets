package pl.polgrabiat.websockets.chat.websocketschat.dto;

import org.springframework.web.socket.WebSocketSession;

import java.time.LocalDateTime;

public class UserCtx {
    private final WebSocketSession session;
    private String nick;
    private String userName;
    private String realName;
    private int lastPongNumber;
    private LocalDateTime lastPongTime;

    public UserCtx(WebSocketSession session) {
        this.session = session;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public WebSocketSession getSession() {
        return session;
    }

    public int getLastPongNumber() {
        return lastPongNumber;
    }

    public void setLastPongNumber(int lastPongNumber) {
        this.lastPongNumber = lastPongNumber;
    }

    public LocalDateTime getLastPongTime() {
        return lastPongTime;
    }

    public void setLastPongTime(LocalDateTime lastPongTime) {
        this.lastPongTime = lastPongTime;
    }
}
