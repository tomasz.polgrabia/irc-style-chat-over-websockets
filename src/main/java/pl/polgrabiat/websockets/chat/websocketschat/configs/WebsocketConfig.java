package pl.polgrabiat.websockets.chat.websocketschat.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import pl.polgrabiat.websockets.chat.websocketschat.handlers.ChatWebsocketHandler;

import javax.inject.Inject;

@Configuration
@EnableWebSocket
@EnableScheduling
@EnableAsync
public class WebsocketConfig implements WebSocketConfigurer {

    @Inject
    private ChatWebsocketHandler websocketController;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(websocketController,"/chat").withSockJS();
    }
}
