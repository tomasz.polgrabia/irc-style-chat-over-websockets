package pl.polgrabiat.websockets.chat.websocketschat.handlers;

import org.springframework.web.socket.WebSocketSession;
import pl.polgrabiat.websockets.chat.websocketschat.dto.ChannelCtx;
import pl.polgrabiat.websockets.chat.websocketschat.dto.GlobalCtx;
import pl.polgrabiat.websockets.chat.websocketschat.dto.UserCtx;

import java.io.IOException;
import java.util.StringTokenizer;

public class PrivMessageHandler implements SessionCommandHandler {
    @Override
    public boolean handleCommand(GlobalCtx globalCtx, WebSocketSession session, String payload, String command, String data) throws IOException {
        StringTokenizer tokenizer = new StringTokenizer(data);
        String destination = tokenizer.nextToken();
        UserCtx userCtx = globalCtx.getUserSessions().get(session);

        if (destination.startsWith("#")) {
            // channel

            ChannelCtx channel = globalCtx.getChannels().get(destination);
            if (channel == null) {
                sendMessage(session,
                        globalCtx, userCtx.getNick(),"MSG invalid destination");
                return true;
            }

            if (!channel.getUsers().contains(userCtx)) {
                sendMessage(session, globalCtx, userCtx.getNick(), "MSG You are not the member of this channel");
                return true;
            }

            // sending to other users

            for (UserCtx user : channel.getUsers()) {
                sendMessage(user.getSession(), globalCtx, userCtx.getNick(), payload);
            }

            return true;

        } else {
            // user


            UserCtx destUserCtx = globalCtx.getUserSessions().get(globalCtx.getUserNameSessions().get(destination));
            UserCtx sourceUserCtx = globalCtx.getUserSessions().get(session);
            if (destUserCtx == null) {
                sendMessage(session,
                        globalCtx, destination,"MSG invalid destination");
                return true;
            }

            sendMessage(destUserCtx.getSession(),
                    globalCtx, sourceUserCtx.getNick(), payload); // destination and source
            sendMessage(session,
                    globalCtx, sourceUserCtx.getNick(), payload); // destination and source

            return true;
        }
    }
}
