package pl.polgrabiat.websockets.chat.websocketschat.dto;

import java.util.HashSet;
import java.util.Set;

public class ChannelCtx {
    private String name;
    private Set<UserCtx> users = new HashSet<>();
    private Set<ChannelMode> modes = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserCtx> getUsers() {
        return users;
    }

    public void setUsers(Set<UserCtx> users) {
        this.users = users;
    }

    public Set<ChannelMode> getModes() {
        return modes;
    }

    public void setModes(Set<ChannelMode> modes) {
        this.modes = modes;
    }
}
