package pl.polgrabiat.websockets.chat.websocketschat.handlers;

import org.springframework.web.socket.WebSocketSession;
import pl.polgrabiat.websockets.chat.websocketschat.dto.GlobalCtx;

import java.io.IOException;

public class LeaveCommandHandler implements SessionCommandHandler {
    @Override
    public boolean handleCommand(GlobalCtx globalCtx, WebSocketSession session, String payload, String command, String data) throws IOException {
        return false;
    }
}
