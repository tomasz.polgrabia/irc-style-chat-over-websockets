package pl.polgrabiat.websockets.chat.websocketschat.dto;

import java.util.Arrays;
import java.util.Optional;

public enum ChannelMode {
    PRIVATE("priv"),
    PASSWORD_PROTECTED("pass"),
    REGISTERED("reg");

    private final String pass;

    ChannelMode(String pass) {
        this.pass = pass;
    }

    public String getPass() {
        return pass;
    }

    public static Optional<ChannelMode> fromCode(String code) {
        return Arrays.stream(values()).filter(it -> it.pass.equals(code)).findFirst();
    }
}
