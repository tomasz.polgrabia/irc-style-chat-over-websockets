package pl.polgrabiat.websockets.chat.websocketschat.dto;

import org.springframework.web.socket.WebSocketSession;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GlobalCtx {
    private Set<WebSocketSession> sessions = new HashSet<>();
    private Map<WebSocketSession, UserCtx> userSessions = new HashMap<>();
    private String serverName = "localhost";
    private Map<String, ChannelCtx> channels = new HashMap<>();
    private Map<String, WebSocketSession> userNameSessions = new HashMap<>();

    public Set<WebSocketSession> getSessions() {
        return sessions;
    }

    public void setSessions(Set<WebSocketSession> sessions) {
        this.sessions = sessions;
    }

    public Map<WebSocketSession, UserCtx> getUserSessions() {
        return userSessions;
    }

    public void setUserSessions(Map<WebSocketSession, UserCtx> userSessions) {
        this.userSessions = userSessions;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public Map<String, ChannelCtx> getChannels() {
        return channels;
    }

    public void setChannels(Map<String, ChannelCtx> channels) {
        this.channels = channels;
    }

    public Map<String, WebSocketSession> getUserNameSessions() {
        return userNameSessions;
    }

    public void setUserNameSessions(Map<String, WebSocketSession> userNameSessions) {
        this.userNameSessions = userNameSessions;
    }

    synchronized public void removeSession(WebSocketSession session) {
        sessions.remove(session);
        UserCtx userCtx = userSessions.get(session);
        userSessions.remove(userSessions);
        userNameSessions.remove(userCtx.getNick());
    }

    synchronized public void addSession(WebSocketSession session) {
        sessions.add(session);
        userSessions.put(session, new UserCtx(session));
    }
}
