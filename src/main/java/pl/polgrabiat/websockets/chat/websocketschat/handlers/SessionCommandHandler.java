package pl.polgrabiat.websockets.chat.websocketschat.handlers;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import pl.polgrabiat.websockets.chat.websocketschat.dto.GlobalCtx;

import java.io.IOException;

public interface SessionCommandHandler {
    boolean handleCommand(GlobalCtx globalCtx, WebSocketSession session, String payload, String command, String data) throws IOException;

    default void sendMessage(WebSocketSession session, GlobalCtx globalCtx, String source, String data) throws IOException {
        session.sendMessage(new TextMessage(String.format(":%s %s %s",
                globalCtx.getServerName(),
                source,
                data)));
    }
}
