package pl.polgrabiat.websockets.chat.websocketschat.handlers;

import org.springframework.web.socket.WebSocketSession;
import pl.polgrabiat.websockets.chat.websocketschat.dto.GlobalCtx;
import pl.polgrabiat.websockets.chat.websocketschat.dto.UserCtx;

import java.io.IOException;
import java.util.StringTokenizer;

public class NickCommandHandler implements SessionCommandHandler {
    @Override
    public boolean handleCommand(GlobalCtx globalCtx, WebSocketSession session, String payload, String command, String data) throws IOException {
        StringTokenizer tokenizer = new StringTokenizer(data);
        String nick = tokenizer.nextToken();

        if (nick.equals(globalCtx.getUserSessions().get(session).getNick())) {
          sendMessage(session,globalCtx, globalCtx.getServerName(),"MSG nick already used");
          return true;
        }

        UserCtx userCtx = globalCtx.getUserSessions().get(session);
        if (userCtx.getNick() != null) {
            globalCtx.getUserNameSessions().remove(userCtx.getNick());
            // removing old mapping for destinations
        }

        userCtx.setNick(nick);
        globalCtx.getUserNameSessions().put(nick, session);

        return true;
    }
}
