package pl.polgrabiat.websockets.chat.websocketschat.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;
import pl.polgrabiat.websockets.chat.websocketschat.dto.ChannelCtx;
import pl.polgrabiat.websockets.chat.websocketschat.dto.GlobalCtx;
import pl.polgrabiat.websockets.chat.websocketschat.dto.UserCtx;

import java.io.IOException;
import java.util.StringTokenizer;

public class JoinCommandHandler implements SessionCommandHandler {

    private static final Logger lg = LoggerFactory.getLogger(JoinCommandHandler.class);

    @Override
    public boolean handleCommand(GlobalCtx globalCtx, WebSocketSession session, String payload, String command, String data) throws IOException {
        StringTokenizer tokenizer = new StringTokenizer(data);
        String destination = tokenizer.nextToken().trim();

        ChannelCtx channelCtx = globalCtx.getChannels().get(destination);
        if (channelCtx == null) {
            channelCtx = new ChannelCtx();
            channelCtx.setName(destination);
            globalCtx.getChannels().put(destination, channelCtx);
        }

        UserCtx userCtx = globalCtx.getUserSessions().get(session);
        channelCtx.getUsers().add(userCtx);

        for (UserCtx user : channelCtx.getUsers()) {
            try {
                sendMessage(user.getSession(), globalCtx,
                        destination,
                        "PRIVMSG " + channelCtx.getName() + " "
                                + userCtx.getNick() + " has "
                                + " joined the channel");
            } catch (RuntimeException e) {
                lg.error("Error while sending a message", e);
            }
        }

        return true;
    }
}
