package pl.polgrabiat.websockets.chat.websocketschat.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;
import pl.polgrabiat.websockets.chat.websocketschat.dto.GlobalCtx;
import pl.polgrabiat.websockets.chat.websocketschat.dto.UserCtx;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.StringTokenizer;

public class PongMessageHandler implements SessionCommandHandler {


    private static final Logger lg = LoggerFactory.getLogger(PongMessageHandler.class);

    @Override
    public boolean handleCommand(GlobalCtx globalCtx, WebSocketSession session, String payload, String command, String data) throws IOException {
        UserCtx userCtx = globalCtx.getUserSessions().get(session);
        if (userCtx == null) {
            lg.error("We lost an user context - dropping session, sorry...");
            globalCtx.removeSession(session);
            return true;
        }

        StringTokenizer tokenizer = new StringTokenizer(data);
        String pongValue = tokenizer.nextToken(":");

        lg.debug("Got pong value: {} for {}", pongValue, session);
        try {
            int pongVal = Integer.parseInt(pongValue);
            if (userCtx.getLastPongNumber() == pongVal) {
                userCtx.setLastPongTime(LocalDateTime.now());
            }
        } catch (NumberFormatException e) {
            lg.debug("Invalid format of pong message. Dropping session", e);
            globalCtx.removeSession(session);
        }

        return true;
    }
}
