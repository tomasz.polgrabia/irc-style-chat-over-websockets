package pl.polgrabiat.websockets.chat.websocketschat.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.PongMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import pl.polgrabiat.websockets.chat.websocketschat.dto.GlobalCtx;
import pl.polgrabiat.websockets.chat.websocketschat.dto.UserCtx;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Component
public class ChatWebsocketHandler extends TextWebSocketHandler {

    private static final Logger lg = LoggerFactory.getLogger(ChatWebsocketHandler.class);
    private static final long PING_PONG_TIME = 59;

    private Map<String, SessionCommandHandler> commandHandlers = new HashMap<>();
    private GlobalCtx globalCtx = new GlobalCtx();
    private Random rnd = new Random();

    public ChatWebsocketHandler() {
        commandHandlers.put("USER", new UserCommandHandler());
        commandHandlers.put("NICK", new NickCommandHandler());
        commandHandlers.put("JOIN", new JoinCommandHandler());
        commandHandlers.put("LEAVE", new LeaveCommandHandler());
        commandHandlers.put("PRIVMSG", new PrivMessageHandler());
        commandHandlers.put("PONG", new PongMessageHandler());
        commandHandlers.put("PING", new PingMessageHandler());
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);

        globalCtx.addSession(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        globalCtx.removeSession(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        super.handleTextMessage(session, message);

        lg.info("Message received: {}", message);
        String payload = message.getPayload();
        int commandEndIdx = payload.indexOf(" ");
        if (commandEndIdx < 0) {
            // no command
            session.sendMessage(new TextMessage("Invalid command " + payload));
            return;
        }
        String command = payload.substring(0, commandEndIdx);
        String data = payload.substring(commandEndIdx+1);
        SessionCommandHandler commandHandler = commandHandlers.get(command.toUpperCase());
        if (commandHandler != null) {
            commandHandler.handleCommand(globalCtx, session, payload, command, data);
            return;
        }

        session.sendMessage(new TextMessage("Invalid command " + payload));
    }


    @Scheduled(fixedDelay=60000)
    public void checkSessionActiveness() {
        LocalDateTime time = LocalDateTime.now();
        lg.debug("Checking sessions with set local date time {} ...", time);
        for (WebSocketSession session: globalCtx.getSessions()) {
            try {
                UserCtx userCtx = globalCtx.getUserSessions().get(session);
                LocalDateTime lastPongTime = userCtx.getLastPongTime();
                if (lastPongTime != null
                        && lastPongTime.until(LocalDateTime.now(), ChronoUnit.SECONDS) >= PING_PONG_TIME) {
                    // drop session
                    lg.trace("Closing session {}[{}]", session.getId(), session.getRemoteAddress());
                    session.close();
                    globalCtx.removeSession(session);
                    continue;
                }

                int pongNumber = rnd.nextInt();
                lg.trace("Sending ping message {} to the session {}[{}]",
                        pongNumber,
                        session.getId(),
                        session.getRemoteAddress());

                session.sendMessage(new TextMessage("PING :" + pongNumber));
                userCtx.setLastPongNumber(pongNumber);

            } catch (IOException e) {
                lg.warn("I/O error", e);
            }
        }
    }

}
