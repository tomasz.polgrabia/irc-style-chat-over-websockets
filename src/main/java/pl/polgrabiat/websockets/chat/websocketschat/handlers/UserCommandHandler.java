package pl.polgrabiat.websockets.chat.websocketschat.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;
import pl.polgrabiat.websockets.chat.websocketschat.dto.GlobalCtx;
import pl.polgrabiat.websockets.chat.websocketschat.dto.UserCtx;

import java.io.IOException;
import java.util.StringTokenizer;

public class UserCommandHandler implements SessionCommandHandler {

    private static final Logger lg = LoggerFactory.getLogger(UserCommandHandler.class);

    @Override
    public boolean handleCommand(GlobalCtx globalCtx, WebSocketSession session, String payload, String command, String data) throws IOException {
        StringTokenizer tokenizer = new StringTokenizer(data, " ");
        String userName = tokenizer.nextToken();
        String code1 = tokenizer.nextToken();
        if (!"0".equals(code1)) {
            return false;
        }

        String code2 = tokenizer.nextToken();
        if (!"*".equals(code2)) {
            return false;
        }

        String realName = tokenizer.nextToken(":");

        UserCtx userCtx = globalCtx.getUserSessions().get(session);

        if (userCtx == null) {
            sendMessage(session,
                    globalCtx,
                    globalCtx.getUserSessions().get(session).getNick(),
                    "MSG you nead to select the nick");
            return true;
        }

        userCtx.setUserName(userName);
        userCtx.setRealName(realName);

        sendMessage(session, globalCtx,
                globalCtx.getUserSessions().get(session).getNick(),
                "PRIVMSG " + userCtx.getNick() + " hello");
        return true;
    }
}
