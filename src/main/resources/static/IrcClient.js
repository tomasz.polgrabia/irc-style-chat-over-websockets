function tabRef (destination) {
    if (destination.startsWith("#")) {
        return "c" + destination.substr(1);
    } else {
        return "u" + destination;
    }
};

function nextToken(data, token) {
    var idx = data.indexOf(token);
    return data.substr(idx+1);
}

function currentToken(data, token) {
    var idx = data.indexOf(token);
    return data.substr(0, idx);
}

function IrcClient(sockJs, nick, userName, realName) {
    this.sockJs = sockJs;
    this.nick = nick;
    this.userName = userName;
    this.realName = realName;

    this.sendMsg = function (msg) {
        console.log("OUT-MSG " + msg);
        this.sockJs.send(msg);
    };

    this.sendNickMsg = function () {
        this.sendMsg("NICK " + this.nick);
    };

    this.sendUserMsg = function () {
        this.sendMsg("USER " + this.userName + " 0 * :" + this.realName);
    };

    this.sendPrivMsg = function (msg) {
        this.sendDestPrivMsg(this.selectedDestination(), msg);
    };

    this.sendDestPrivMsg = function (dest, msg) {
        this.sendMsg("PRIVMSG " + dest + " " + msg);
    };

    this.init = function () {
        this.sendNickMsg();
        this.sendUserMsg();
    };

    var ircClientRef = this;

    this.sockJs.onopen = function () {
        console.log("Socket opened");
        ircClientRef.init();
    };

    this.sockJs.onclose = function () {
        console.log("Socket closed");
    };

    this.sendPongMsg = function (nr) {
        this.sendMsg("PONG :" + nr);
    };

    this.handlePongMessage = function (payload, data) {
        console.log("PING message: " + data);
        this.sendPongMsg(data);
    };

    this.createTabEl = function (destination) {
        var tabs = $("#tabs");
        var liEl = $("<li/>");
        var aEl = $("<a/>");

        aEl.attr("href", "#" + tabRef(destination));
        aEl.attr("data-toggle", "tab");
        aEl.html(destination);


        liEl.attr("id", "label-" + tabRef(destination));
        liEl.append(aEl);
        tabs.append(liEl);
    };

    this.createTabContentEl = function (destination) {
        var tabsContent = $("#tabs-content");
        var divEl = $("<div />");
        divEl.addClass("tab-pane fade in");
        divEl.attr("id", tabRef(destination));
        tabRef(destination);

        tabsContent.append(divEl);
    };

    this.checkForDestElseCreate = function (destination) {
        var tabIdx = tabRef(destination);
        if ($("#label-" + tabIdx).length > 0) {
            return;
        }

        ircClientRef.createTabEl(destination);
        ircClientRef.createTabContentEl(destination);
    };

    this.displayDestinationMessage = function (destination, msg) {
        this.checkForDestElseCreate(destination);
        var server = $("#" + tabRef(destination));
        var logRow = $("<div/>");
        var source = currentToken(nextToken(msg, " "), " ");
        var privMsgCmd = "PRIVMSG";
        var idx = msg.toUpperCase().indexOf(privMsgCmd);
        var msgDestData = msg.substr(idx+privMsgCmd.length+1);
        var msgData = nextToken(msgDestData, " ");
        logRow.text("<" + source + "> " + msgData);
        server.append(logRow);
        return server;
    };

    this.selectedDestination = function () {
        return $("#tabs").find("li.active").text();
    };

    this.fetchDestination = function (data) {
        // var server = currentToken(data, " ");
        var source = nextToken(data, " ");
        var command = nextToken(source, " ");
        var dest = nextToken(command, " ");
        return currentToken(dest, " ");
    };

    this.fetchSource = function (data) {
        sourceData = nextToken(data, " ");
        return sourceData.substr(0, sourceData.indexOf(" ")+1).trim();
    };

    this.sockJs.onmessage = function (e) {
        var msg = e.data;
        if (msg.startsWith("PING ")) {
            ircClientRef.handlePongMessage(msg, msg.substr(7));
            return;
        }

        console.log("Received message: " + msg);
        var destination = ircClientRef.fetchDestination(e.data);
        var source = ircClientRef.fetchSource(e.data);
        var tabName = ircClientRef.nick == destination ? source : destination;
        console.log("Destination of the message: " + destination);

        var server = ircClientRef.displayDestinationMessage(tabName, msg);
    };

    this.createUserTab = function(dest) {
        this.checkForDestElseCreate(dest);
    };

};