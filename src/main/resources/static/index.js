window.initSockJs = function () {
    window.sockJs = null;
    window.nick = null;
    bootbox.prompt({
        title: "Type the nick name for the chat",
        inputType: 'text',
        callback: function (result) {
            console.log("Got nick: " + result);
            window.nick = result;

            if (window.nick == null || window.nick == undefined || window.nick.length < 1) {
                console.log("Nick cannot be blank");
                bootbox.alert("Sorry, nick the cannot be blank", function (res) {
                    initSockJs();
                });
                return;
            }

            window.sockJs = new SockJS("/chat");
            window.ircClient = new IrcClient(window.sockJs, window.nick, window.nick, window.nick);
        }
    });
};

window.onload = function () {
    this.initSockJs();

    var messageInput = document.getElementById("message");
    messageInput.onkeyup = function (e) {
        if (e.keyCode == 13) {
            var command = messageInput.value;
            if (command.startsWith("/")) {
                // special command
                if (command.toUpperCase().startsWith("/QUERY")) {
                    window.ircClient.createUserTab(command.substr(6).trim());
                } else {
                    window.ircClient.sendMsg(command.substr(1));
                }
            } else {
                window.ircClient.sendPrivMsg(command);
            }
            $(messageInput).val("");
        }
    };
};