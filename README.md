# Purpose of this project

It's one of my old projects just to get used to a new technology - websockets. It's pretty functional in the simplest
use case - chatting to one another with a simple irc style functionality, no authorisation as it used to be
popular in the old irc times.

# How to run it

## Environment
$env:JAVA_HOME="C:\developer\bin\sdks\jdk-8.0.252.09-hotspot"

It doesn't mean that it needs to be java 8 but it was tested running with that version.

## Bootstrapping procedure
./gradlew bootRun 

It works both on Linux and Windows
